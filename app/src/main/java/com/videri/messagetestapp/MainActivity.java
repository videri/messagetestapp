package com.videri.messagetestapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.videri.canvas.sdk.CanvasControlMsgParcelable;
import com.videri.canvas.sdk.CanvasOrchestrationMsgItem;
import com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable;
import com.videri.canvas.sdk.ICanvasAppRemoteService;
import com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.videri.canvas.sdk.CanvasControlMsgItem.TypeString;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    private String PACKAGE_NAME;

    private String sendMsg = "";

    private String receivedMsg = "";

    private ICanvasAppRemoteService mRemoteService;

    private ICanvasAppRemoteServiceCallback.Stub mCallback = new ICanvasAppRemoteServiceCallback.Stub() {
        @Override
        public void dataReceived(CanvasControlMsgParcelable dataObj) throws RemoteException {
            Log.i(TAG, "" + dataObj.messages.length);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    receiverText.setText("dataReceived========================" + getCurrentTime());
                }
            });
//            generateAndSend749();

        }

        @Override
        public void orchestrationDataReceived(final CanvasOrchestrationMsgParcelable message) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    receiverText.setText("orchestrationDataReceived========================" + getCurrentTime());
                }
            });
//            displayReceived749(dataObj);
//            if (message != null && message.messages != null && message.messages.length > 0) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        receiverText.setText("orchestrationDataReceived: " + message.messages[0].valueString);
//                    }
//                });
//            }
        }


    };


    private Context mContext;

    private TextView senderText;

    private TextView receiverText;


    private Timer mTimer = new Timer("MessageTestTimer");


    private TimerTask dumpHPROFTask = new TimerTask() {
        @Override
        public void run() {
            try {
                String msg = "A test message sent on: " + getCurrentTime();
                CanvasOrchestrationMsgParcelable cp = new CanvasOrchestrationMsgParcelable();
                cp.messages = new CanvasOrchestrationMsgItem[1];
                CanvasOrchestrationMsgItem item = new CanvasOrchestrationMsgItem();
                item.typeObj = TypeString;
                item.valueString = msg;
                item.description = "lucky number";
                cp.messages[0] = item;
                mRemoteService.canvasAppMsgToOrchestrationGroup(PACKAGE_NAME, cp);
                final String msgText = "show cp's text: " + cp.messages[0].valueString;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        senderText.setText(msgText);
                    }
                });
            } catch (RemoteException e) {
                Toast.makeText(mContext, "sending remote message failed. " + e.toString(),Toast.LENGTH_LONG).show();
            }
//            OomExceptionHandler.generateProfile("com.honda.tjba.tuner");
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        senderText = (TextView)findViewById(R.id.sender_text);

        receiverText = (TextView)findViewById(R.id.receiver_text);

        PACKAGE_NAME = getApplicationContext().getPackageName();
        Toast.makeText(this, "Going to register canvas service with:  "+ PACKAGE_NAME, Toast.LENGTH_SHORT).show();
        Intent serviceIntent = new Intent();
        serviceIntent.setClassName("com.videri.canvas", "com.videri.canvas.CanvasApp.CanvasAppService");
        bindService(serviceIntent, mConnection, BIND_AUTO_CREATE);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mRemoteService = ICanvasAppRemoteService.Stub.asInterface(service);
            if (mRemoteService != null)
                try {
                    Toast.makeText(mContext, "Service connected with:  "+ PACKAGE_NAME, Toast.LENGTH_LONG).show();
                    mTimer.scheduleAtFixedRate( dumpHPROFTask,0l,10000 );
                    mRemoteService.registerCallBack(PACKAGE_NAME, mCallback);
                    Log.i("WatchApp", "register callback");
                } catch (RemoteException e) {
                    Log.e("WatchApp", "Exception register callback");
                }
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            try {
                mRemoteService.unRegisterCallBack(PACKAGE_NAME);
                mTimer.cancel();
            } catch (RemoteException e) {
            }
            mRemoteService = null;
        }
    };



    private static String getCurrentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd:HH:mm:ss");
        String time = sdf.format(new Date());
        return time;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTimer.cancel();
    }
}
