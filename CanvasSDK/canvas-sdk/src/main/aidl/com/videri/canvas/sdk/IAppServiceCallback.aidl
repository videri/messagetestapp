// IAppServiceCallback.aidl
package com.videri.canvas.sdk;

interface IAppServiceCallback {
    oneway void ready();
    oneway void error(String header, String message);
    oneway void destroyed();
    oneway void log(String level, String tag, String message);
}
