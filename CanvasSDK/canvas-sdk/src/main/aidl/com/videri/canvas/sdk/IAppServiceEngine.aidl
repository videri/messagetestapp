// IAppServiceEngine.aidl
package com.videri.canvas.sdk;

import android.os.Bundle;
import android.view.Surface;

import com.videri.canvas.sdk.IAppServiceCallback;

interface IAppServiceEngine {
    void create(in IAppServiceCallback callback, in int width, in int height, inout Surface surface, in long startTime);
    void stop();

    boolean handlesDisplaySubregion();

    // TODO Needs more params to define position and size of content
    void setDisplaySubregion(in float x, in float y, in float width, in float height);
}
