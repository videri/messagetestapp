package com.videri.canvas.sdk;

public interface Metrics {
    /**
     * Get the lower of the screen width and height in pixels
     *
     * @return
     */
    int getScreenWidth();

    /**
     * Get the larger of the screen width and height in pixels
     *
     * @return
     */
    int getScreenHeight();
}
