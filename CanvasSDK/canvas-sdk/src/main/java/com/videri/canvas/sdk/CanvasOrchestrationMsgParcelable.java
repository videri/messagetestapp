package com.videri.canvas.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public class CanvasOrchestrationMsgParcelable implements Parcelable {

    public CanvasOrchestrationMsgItem[] messages;

    public CanvasOrchestrationMsgParcelable() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(messages, flags);
    }

    private CanvasOrchestrationMsgParcelable(Parcel source) {
        readParcel(source);
    }

    private void readParcel(Parcel source) {
        messages = source.createTypedArray(CanvasOrchestrationMsgItem.CREATOR);
    }

    public static final Parcelable.Creator<CanvasOrchestrationMsgParcelable> CREATOR = new Parcelable.Creator<CanvasOrchestrationMsgParcelable>() {
        @Override
        public CanvasOrchestrationMsgParcelable createFromParcel(Parcel source) {
            return new CanvasOrchestrationMsgParcelable(source);
        }

        @Override
        public CanvasOrchestrationMsgParcelable[] newArray(int size) {
            return new CanvasOrchestrationMsgParcelable[size];
        }
    };

}
