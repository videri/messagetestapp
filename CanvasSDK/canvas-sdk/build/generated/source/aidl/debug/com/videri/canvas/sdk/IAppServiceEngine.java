/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/ylin/work/videri/MessageTestApp/CanvasSDK/canvas-sdk/src/main/aidl/com/videri/canvas/sdk/IAppServiceEngine.aidl
 */
package com.videri.canvas.sdk;
public interface IAppServiceEngine extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.videri.canvas.sdk.IAppServiceEngine
{
private static final java.lang.String DESCRIPTOR = "com.videri.canvas.sdk.IAppServiceEngine";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.videri.canvas.sdk.IAppServiceEngine interface,
 * generating a proxy if needed.
 */
public static com.videri.canvas.sdk.IAppServiceEngine asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.videri.canvas.sdk.IAppServiceEngine))) {
return ((com.videri.canvas.sdk.IAppServiceEngine)iin);
}
return new com.videri.canvas.sdk.IAppServiceEngine.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_create:
{
data.enforceInterface(DESCRIPTOR);
com.videri.canvas.sdk.IAppServiceCallback _arg0;
_arg0 = com.videri.canvas.sdk.IAppServiceCallback.Stub.asInterface(data.readStrongBinder());
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
android.view.Surface _arg3;
if ((0!=data.readInt())) {
_arg3 = android.view.Surface.CREATOR.createFromParcel(data);
}
else {
_arg3 = null;
}
long _arg4;
_arg4 = data.readLong();
this.create(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
if ((_arg3!=null)) {
reply.writeInt(1);
_arg3.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_stop:
{
data.enforceInterface(DESCRIPTOR);
this.stop();
reply.writeNoException();
return true;
}
case TRANSACTION_handlesDisplaySubregion:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.handlesDisplaySubregion();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_setDisplaySubregion:
{
data.enforceInterface(DESCRIPTOR);
float _arg0;
_arg0 = data.readFloat();
float _arg1;
_arg1 = data.readFloat();
float _arg2;
_arg2 = data.readFloat();
float _arg3;
_arg3 = data.readFloat();
this.setDisplaySubregion(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.videri.canvas.sdk.IAppServiceEngine
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void create(com.videri.canvas.sdk.IAppServiceCallback callback, int width, int height, android.view.Surface surface, long startTime) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
_data.writeInt(width);
_data.writeInt(height);
if ((surface!=null)) {
_data.writeInt(1);
surface.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeLong(startTime);
mRemote.transact(Stub.TRANSACTION_create, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
surface.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void stop() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stop, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public boolean handlesDisplaySubregion() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_handlesDisplaySubregion, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// TODO Needs more params to define position and size of content

@Override public void setDisplaySubregion(float x, float y, float width, float height) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeFloat(x);
_data.writeFloat(y);
_data.writeFloat(width);
_data.writeFloat(height);
mRemote.transact(Stub.TRANSACTION_setDisplaySubregion, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_create = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_stop = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_handlesDisplaySubregion = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_setDisplaySubregion = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
public void create(com.videri.canvas.sdk.IAppServiceCallback callback, int width, int height, android.view.Surface surface, long startTime) throws android.os.RemoteException;
public void stop() throws android.os.RemoteException;
public boolean handlesDisplaySubregion() throws android.os.RemoteException;
// TODO Needs more params to define position and size of content

public void setDisplaySubregion(float x, float y, float width, float height) throws android.os.RemoteException;
}
