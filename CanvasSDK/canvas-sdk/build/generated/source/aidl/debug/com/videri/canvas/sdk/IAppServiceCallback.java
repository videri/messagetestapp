/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/ylin/work/videri/MessageTestApp/CanvasSDK/canvas-sdk/src/main/aidl/com/videri/canvas/sdk/IAppServiceCallback.aidl
 */
package com.videri.canvas.sdk;
public interface IAppServiceCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.videri.canvas.sdk.IAppServiceCallback
{
private static final java.lang.String DESCRIPTOR = "com.videri.canvas.sdk.IAppServiceCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.videri.canvas.sdk.IAppServiceCallback interface,
 * generating a proxy if needed.
 */
public static com.videri.canvas.sdk.IAppServiceCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.videri.canvas.sdk.IAppServiceCallback))) {
return ((com.videri.canvas.sdk.IAppServiceCallback)iin);
}
return new com.videri.canvas.sdk.IAppServiceCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_ready:
{
data.enforceInterface(DESCRIPTOR);
this.ready();
return true;
}
case TRANSACTION_error:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
this.error(_arg0, _arg1);
return true;
}
case TRANSACTION_destroyed:
{
data.enforceInterface(DESCRIPTOR);
this.destroyed();
return true;
}
case TRANSACTION_log:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
this.log(_arg0, _arg1, _arg2);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.videri.canvas.sdk.IAppServiceCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void ready() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_ready, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void error(java.lang.String header, java.lang.String message) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(header);
_data.writeString(message);
mRemote.transact(Stub.TRANSACTION_error, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void destroyed() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_destroyed, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void log(java.lang.String level, java.lang.String tag, java.lang.String message) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(level);
_data.writeString(tag);
_data.writeString(message);
mRemote.transact(Stub.TRANSACTION_log, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_ready = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_error = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_destroyed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_log = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
public void ready() throws android.os.RemoteException;
public void error(java.lang.String header, java.lang.String message) throws android.os.RemoteException;
public void destroyed() throws android.os.RemoteException;
public void log(java.lang.String level, java.lang.String tag, java.lang.String message) throws android.os.RemoteException;
}
