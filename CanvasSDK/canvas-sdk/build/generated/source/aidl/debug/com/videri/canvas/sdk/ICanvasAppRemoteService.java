/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/ylin/work/videri/MessageTestApp/CanvasSDK/canvas-sdk/src/main/aidl/com/videri/canvas/sdk/ICanvasAppRemoteService.aidl
 */
package com.videri.canvas.sdk;
public interface ICanvasAppRemoteService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.videri.canvas.sdk.ICanvasAppRemoteService
{
private static final java.lang.String DESCRIPTOR = "com.videri.canvas.sdk.ICanvasAppRemoteService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.videri.canvas.sdk.ICanvasAppRemoteService interface,
 * generating a proxy if needed.
 */
public static com.videri.canvas.sdk.ICanvasAppRemoteService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.videri.canvas.sdk.ICanvasAppRemoteService))) {
return ((com.videri.canvas.sdk.ICanvasAppRemoteService)iin);
}
return new com.videri.canvas.sdk.ICanvasAppRemoteService.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_registerCallBack:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback _arg1;
_arg1 = com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback.Stub.asInterface(data.readStrongBinder());
this.registerCallBack(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_unRegisterCallBack:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.unRegisterCallBack(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_getCallBack:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback _result = this.getCallBack(_arg0);
reply.writeNoException();
reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
return true;
}
case TRANSACTION_canvasAppResponse:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.videri.canvas.sdk.CanvasControlMsgParcelable _arg1;
if ((0!=data.readInt())) {
_arg1 = com.videri.canvas.sdk.CanvasControlMsgParcelable.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.canvasAppResponse(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_canvasAppMsgToOrchestrationGroup:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable _arg1;
if ((0!=data.readInt())) {
_arg1 = com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.canvasAppMsgToOrchestrationGroup(_arg0, _arg1);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.videri.canvas.sdk.ICanvasAppRemoteService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void registerCallBack(java.lang.String pkgName, com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pkgName);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerCallBack, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void unRegisterCallBack(java.lang.String pkgName) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pkgName);
mRemote.transact(Stub.TRANSACTION_unRegisterCallBack, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback getCallBack(java.lang.String pkgName) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pkgName);
mRemote.transact(Stub.TRANSACTION_getCallBack, _data, _reply, 0);
_reply.readException();
_result = com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback.Stub.asInterface(_reply.readStrongBinder());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public void canvasAppResponse(java.lang.String pkgName, com.videri.canvas.sdk.CanvasControlMsgParcelable response) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pkgName);
if ((response!=null)) {
_data.writeInt(1);
response.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_canvasAppResponse, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void canvasAppMsgToOrchestrationGroup(java.lang.String pkgName, com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable message) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pkgName);
if ((message!=null)) {
_data.writeInt(1);
message.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_canvasAppMsgToOrchestrationGroup, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_registerCallBack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_unRegisterCallBack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getCallBack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_canvasAppResponse = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_canvasAppMsgToOrchestrationGroup = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
}
public void registerCallBack(java.lang.String pkgName, com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback callback) throws android.os.RemoteException;
public void unRegisterCallBack(java.lang.String pkgName) throws android.os.RemoteException;
public com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback getCallBack(java.lang.String pkgName) throws android.os.RemoteException;
public void canvasAppResponse(java.lang.String pkgName, com.videri.canvas.sdk.CanvasControlMsgParcelable response) throws android.os.RemoteException;
public void canvasAppMsgToOrchestrationGroup(java.lang.String pkgName, com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable message) throws android.os.RemoteException;
}
