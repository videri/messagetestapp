/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/ylin/work/videri/MessageTestApp/CanvasSDK/canvas-sdk/src/main/aidl/com/videri/canvas/sdk/IAppService.aidl
 */
package com.videri.canvas.sdk;
public interface IAppService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.videri.canvas.sdk.IAppService
{
private static final java.lang.String DESCRIPTOR = "com.videri.canvas.sdk.IAppService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.videri.canvas.sdk.IAppService interface,
 * generating a proxy if needed.
 */
public static com.videri.canvas.sdk.IAppService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.videri.canvas.sdk.IAppService))) {
return ((com.videri.canvas.sdk.IAppService)iin);
}
return new com.videri.canvas.sdk.IAppService.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_createEngine:
{
data.enforceInterface(DESCRIPTOR);
android.os.Bundle _arg0;
if ((0!=data.readInt())) {
_arg0 = android.os.Bundle.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
com.videri.canvas.sdk.IAppServiceEngine _result = this.createEngine(_arg0);
reply.writeNoException();
reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.videri.canvas.sdk.IAppService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public com.videri.canvas.sdk.IAppServiceEngine createEngine(android.os.Bundle params) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.videri.canvas.sdk.IAppServiceEngine _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((params!=null)) {
_data.writeInt(1);
params.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_createEngine, _data, _reply, 0);
_reply.readException();
_result = com.videri.canvas.sdk.IAppServiceEngine.Stub.asInterface(_reply.readStrongBinder());
if ((0!=_reply.readInt())) {
params.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_createEngine = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public com.videri.canvas.sdk.IAppServiceEngine createEngine(android.os.Bundle params) throws android.os.RemoteException;
}
