/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/ylin/work/videri/MessageTestApp/CanvasSDK/canvas-sdk/src/main/aidl/com/videri/canvas/sdk/ICanvasAppRemoteServiceCallback.aidl
 */
package com.videri.canvas.sdk;
public interface ICanvasAppRemoteServiceCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback
{
private static final java.lang.String DESCRIPTOR = "com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback interface,
 * generating a proxy if needed.
 */
public static com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback))) {
return ((com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback)iin);
}
return new com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_dataReceived:
{
data.enforceInterface(DESCRIPTOR);
com.videri.canvas.sdk.CanvasControlMsgParcelable _arg0;
if ((0!=data.readInt())) {
_arg0 = com.videri.canvas.sdk.CanvasControlMsgParcelable.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.dataReceived(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_orchestrationDataReceived:
{
data.enforceInterface(DESCRIPTOR);
com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable _arg0;
if ((0!=data.readInt())) {
_arg0 = com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.orchestrationDataReceived(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void dataReceived(com.videri.canvas.sdk.CanvasControlMsgParcelable dataObj) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((dataObj!=null)) {
_data.writeInt(1);
dataObj.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_dataReceived, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void orchestrationDataReceived(com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable dataObj) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((dataObj!=null)) {
_data.writeInt(1);
dataObj.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_orchestrationDataReceived, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_dataReceived = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_orchestrationDataReceived = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
public void dataReceived(com.videri.canvas.sdk.CanvasControlMsgParcelable dataObj) throws android.os.RemoteException;
public void orchestrationDataReceived(com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable dataObj) throws android.os.RemoteException;
}
