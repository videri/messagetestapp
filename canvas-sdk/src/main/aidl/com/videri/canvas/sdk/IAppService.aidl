// IAppService.aidl
package com.videri.canvas.sdk;

import com.videri.canvas.sdk.IAppServiceEngine;

interface IAppService {
    IAppServiceEngine createEngine(inout Bundle params);
}
