// ICanvasRemoteFromAppService.aidl
package com.videri.canvas.sdk;

import com.videri.canvas.sdk.CanvasControlMsgParcelable;
import com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable;
import com.videri.canvas.sdk.ICanvasAppRemoteServiceCallback;

interface ICanvasAppRemoteService {

    void registerCallBack(String pkgName, ICanvasAppRemoteServiceCallback callback);
    void unRegisterCallBack(String pkgName);

    ICanvasAppRemoteServiceCallback getCallBack(String pkgName);
    void canvasAppResponse(String pkgName, in CanvasControlMsgParcelable response);
    void canvasAppMsgToOrchestrationGroup(String pkgName, in CanvasOrchestrationMsgParcelable message);

}
