// ICanvasRemoteToAppService.aidl
package com.videri.canvas.sdk;

import com.videri.canvas.sdk.CanvasControlMsgParcelable;
import com.videri.canvas.sdk.CanvasOrchestrationMsgParcelable;

interface ICanvasAppRemoteServiceCallback {

    void dataReceived(in CanvasControlMsgParcelable dataObj);
    void orchestrationDataReceived(in CanvasOrchestrationMsgParcelable dataObj);

}
