package com.videri.canvas.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public class CanvasControlMsgParcelable implements Parcelable {

    public CanvasControlMsgItem[] messages;

    public CanvasControlMsgParcelable() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(messages, flags);
    }

    private CanvasControlMsgParcelable(Parcel source) {
        readParcel(source);
    }

    private void readParcel(Parcel source) {
        messages = source.createTypedArray(CanvasControlMsgItem.CREATOR);
    }

    public static final Parcelable.Creator<CanvasControlMsgParcelable> CREATOR = new Parcelable.Creator<CanvasControlMsgParcelable>() {
        @Override
        public CanvasControlMsgParcelable createFromParcel(Parcel source) {
            return new CanvasControlMsgParcelable(source);
        }

        @Override
        public CanvasControlMsgParcelable[] newArray(int size) {
            return new CanvasControlMsgParcelable[size];
        }
    };

}
