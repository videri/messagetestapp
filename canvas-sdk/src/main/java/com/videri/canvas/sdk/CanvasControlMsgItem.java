package com.videri.canvas.sdk;


import android.os.Parcel;
import android.os.Parcelable;

public class CanvasControlMsgItem implements Parcelable {
    public final static int TypeUnknown = 1001;
    public final static int TypeBoolean = 0;
    public final static int TypeInt = 1;
    public final static int TypeString = 2;

    public int typeObj = TypeUnknown;
    public String valueString = "";
    public String description = "";


    public CanvasControlMsgItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeObj);
        dest.writeString(valueString);
        dest.writeString(description);
    }

    private CanvasControlMsgItem(Parcel source) {
        readParcel(source);
    }

    private void readParcel(Parcel source) {
        typeObj = source.readInt();
        valueString = source.readString();
        description = source.readString();
    }

    public static final Parcelable.Creator<CanvasControlMsgItem> CREATOR = new Parcelable.Creator<CanvasControlMsgItem>() {
        @Override
        public CanvasControlMsgItem createFromParcel(Parcel source) {
            return new CanvasControlMsgItem(source);
        }

        @Override
        public CanvasControlMsgItem[] newArray(int size) {
            return new CanvasControlMsgItem[size];
        }
    };

}
