package com.videri.canvas.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Choreographer;

import java.util.LinkedList;

public abstract class AppService extends Service {
    private Choreographer choreographer;

    private LinkedList<CanvasApplication> apps;

    public AppService() {
        apps = new LinkedList<>();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        choreographer = Choreographer.getInstance();
    }

    @Override
    public void onLowMemory() {
        Log.v("WHIMAPPSERVICE", "onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onDestroy() {
        Log.v("WHIMAPPSERVICE", "onDestroy");
        synchronized (apps) {
            if (!apps.isEmpty()) {
                for (CanvasApplication application : apps) {
                    application.finish();
                }
            }

            while (!apps.isEmpty()) {
                try {
                    apps.wait();
                } catch (Exception e) {
                }
            }
        }

        super.onDestroy();
    }

    public Choreographer getChoreographer() {
        return choreographer;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        System.exit(0);
        return super.onUnbind(intent);
    }

    void created(CanvasApplication app) {
        Log.v("APPSERVICE", "Created " + app);
        synchronized (apps) {
            apps.add(app);
        }
    }

    void terminated(CanvasApplication app) {
        Log.v("APPSERVICE", "Terminated " + app);
        synchronized (apps) {
            apps.remove(app);
            try {
                apps.notifyAll();
            } catch (Exception e) {
            }
        }
        Log.v("APPSERVICE", "Terminated and removed " + app);
    }

    private final IAppService.Stub binder = new IAppService.Stub() {
        @Override
        public IAppServiceEngine createEngine(Bundle params) throws RemoteException {
            Log.v("WHIMAPPSERVICE", "Creating engine");
            CanvasApplication app = getCanvasApplication(params);

            if (app != null) {
                Log.v("WHIMAPPSERVICE", "Returning engine");
                return app.getStub();
            } else {
                Log.v("WHIMAPPSERVICE", "Error creating engine");
                throw new RemoteException("Unknown type");
            }
        }
    };

    protected IAppService.Stub getBinder() {
        return binder;
    }

    protected abstract CanvasApplication getCanvasApplication(Bundle params);
}
