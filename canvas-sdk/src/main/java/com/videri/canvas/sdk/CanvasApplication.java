package com.videri.canvas.sdk;

import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import android.view.Surface;

// TODO DRM (i.e. can I set an AES crypto decode for a movie manually?)
// TODO multi window support
// TODO if you can't do the AES thing then you might need a global SurfaceView for movies
// and a way to configure the layout of everything
// there was the crazy special VideoView subclass that merely provides coords
// TODO consider if this really gets us anything over the standard marshmallow
// TODO what sort of security to ensure we only connect to valid apphosts and not a fake one?

/**
 *
 */
public abstract class CanvasApplication extends ContextWrapper implements Handler.Callback {
    private IAppServiceEngine.Stub stub;
    private IAppServiceCallback callback;
    private AppService appService;
    private Surface remoteSurface;
    private Bundle params;

    private Object runningLock;
    private boolean running;

    private int width;
    private int height;

    private Metrics metrics;

    private boolean readyNotified;

    private long startTime;

    private Handler handler;

    private static final int MSG_CREATE = 1;
    private static final int MSG_START = 2;
    private static final int MSG_SAVE_STATE = 3;
    private static final int MSG_CONFIGURATION_CHANGED = 4;
    private static final int MSG_RECEIVED = 5;
    private static final int MSG_DESTROY = 6;

    volatile boolean constructing = true;

    public enum CanvasLogLevel {
        VERBOSE,
        DEBUG,
        INFO,
        WARNING,
        ERROR
    }

    protected CanvasApplication(final AppService appService, final Bundle params) {
        super(appService);

        this.appService = appService;
        this.params = params;

        getService().created(this);

        Log.v("WHIMAPP", "Construction starting");

        stub = new IAppServiceEngine.Stub() {
            /**
             * IMPORTANT: this call returns ONLY as mainThread terminates
             *
             * This is to prevent the surface from becoming invalidated when the method
             * returns
             *
             * @param callback
             * @param surface
             * @throws RemoteException
             */
            @Override
            public final void create(IAppServiceCallback callback, int width, int height, Surface surface, long startTime) throws RemoteException {
                Log.v("WHIMAPP", "create starting " + getClass().getSimpleName());

                CanvasApplication.this.startTime = startTime;

                CanvasApplication.this.width = width;
                CanvasApplication.this.height = height;

                remoteSurface = surface;
                CanvasApplication.this.callback = callback;

                if (surface == null) {
                    finish();
                    Log.v("WHIMAPP", "null surface");

                    throw new RuntimeException("Surface is null");
                }

                if (!surface.isValid()) {
                    finish();
                    Log.v("WHIMAPP", "invalid surface");

                    throw new RuntimeException("Surface is not valid");
                }

                Log.v("WHIMAPP", "create sending onStart via Handler " + getClass().getSimpleName());

                try {
                    sendMessage(MSG_CREATE);
                } catch (Throwable t) {
                    throw new RuntimeException("Exception sending create");
                }

                Log.v("WHIMAPP", "create waiting " + getClass().getSimpleName());

                try {
                    // Is something finalizing the surface when this call returns? So we block returning
                    synchronized (runningLock) {
                        // PROBLEM: running is probably false to start with, so use a do-while loop
                        do {
                            try {
                                runningLock.wait();
                            } catch (Throwable o) {
                                if (o instanceof RemoteException) {
                                    running = false;
                                }
                            }
                        } while (running);
                    }
                } catch (Throwable t) {
                    running = false;
                }

                Log.v("WHIMAPP", "create terminating " + getClass().getSimpleName());
            }

            @Override
            public final void stop() throws RemoteException {
                Log.v("WHIMAPP", "Stopping " + getClass().getSimpleName());
                finish();
            }

            @Override
            public boolean handlesDisplaySubregion() throws RemoteException {
                return CanvasApplication.this.handlesDisplaySubregion();
            }

            @Override
            public void setDisplaySubregion(float x, float y, float width, float height) throws RemoteException {
                CanvasApplication.this.setDisplaySubregion(x, y, width, height);
            }
        };

        runningLock = new Object();

        metrics = new Metrics() {
            @Override
            public int getScreenWidth() {
                return Math.min(width, height);
            }

            @Override
            public int getScreenHeight() {
                return Math.max(width, height);
            }
        };

        // This mess is to prevent the create call progressing
        // before our handler and thread are setup properly
        final Object constructorBlock = new Object();

        synchronized (constructorBlock) {
            // Create the actual "main thread" of this app
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Looper.prepare();

                        handler = new Handler(CanvasApplication.this);

                        synchronized (constructorBlock) {
                            try {
                                Log.v("WHIMAPP", "Constructor notify");
                                constructing = false;
                                constructorBlock.notify();
                            } catch (Throwable t) {
                            }
                        }

                        Looper.loop();
                    } catch (Throwable other) {

                    }

                    try {
                        if (callback != null) {
                            callback.destroyed();
                            callback = null;
                        }
                    } catch (Throwable other) {

                    }

                    getService().terminated(CanvasApplication.this);

                    handler = null;
                }
            }).start();

            while (constructing) {
                try {
                    Log.v("WHIMAPP", "Constructor wait");
                    constructorBlock.wait();
                } catch (Throwable o) {
                    constructing = false;
                }
            }
        }

        Log.v("WHIMAPP", "Construction complete");
    }


    public boolean handlesDisplaySubregion() throws RemoteException {
        return false;
    }

    public void setDisplaySubregion(float x, float y, float width, float height) throws RemoteException {

    }

    private final void sendMessage(int what) {
        try {
            if (handler != null) {
                Message m = handler.obtainMessage(what, this);
                handler.sendMessage(m);
            }
        } catch (Throwable t) {
        }
    }

    private void destroyImpl() {
        logCanvas(CanvasLogLevel.VERBOSE, "WHIMCANVAS", "Destroyimpl");

        try {
            // Removing this because it seems to block new stuff
//            Process.setThreadPriority(Process.THREAD_PRIORITY_LESS_FAVORABLE);
            running = false;

            onDestroy();
        } catch (Throwable other) {

        }

        try {
            synchronized (runningLock) {
                running = false;

                try {
                    runningLock.notifyAll();
                } catch (Throwable t) {
                }
            }
        } catch (Throwable other) {

        }

        if (handler != null) {
            handler.getLooper().quit();
        }
    }

    public boolean handleMessage(Message msg) {
        logCanvas(CanvasLogLevel.VERBOSE, "WHIMAPP", "Handling msg " + msg.what + " " + params.toString());

        try {
            switch (msg.what) {
                case MSG_CREATE:
                    Process.setThreadPriority(Process.THREAD_PRIORITY_LESS_FAVORABLE);
                    onCreate(params);
                    // This ensures our start lands on the nose
                    Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_DISPLAY);
                    break;
                case MSG_START:
                    if (!running) {
                        running = true;
                        onStart();
                    }
                    break;
                case MSG_SAVE_STATE:
                    Bundle state = new Bundle();
                    onSaveInstanceState(state);
                    break;
                case MSG_CONFIGURATION_CHANGED:
                    onConfigurationChanged();
                    break;
                case MSG_RECEIVED:
                    onMessageReceived((byte[]) msg.obj);
                    break;
                case MSG_DESTROY:
                    destroyImpl();
                    break;
            }
        } catch (Throwable t) {
            logCanvas(CanvasLogLevel.ERROR, "WHIMAPP", "thrown " + t.toString());
            while (t.getCause() != null) {
                t = t.getCause();
                logCanvas(CanvasLogLevel.ERROR, "WHIMAPP", "thrown by " + t.toString());
            }
        }

        logCanvas(CanvasLogLevel.VERBOSE, "WHIMAPP", "Handled msg " + msg.what + " " + params.toString());
        return true;
    }

    protected AppService getService() {
        return appService;
    }

    protected Surface getSurface() {
        return remoteSurface;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    protected int getWidth() {
        return width;
    }

    protected int getHeight() {
        return height;
    }

    protected boolean isRunning() {
        return running;
    }

    /**
     * Called to setup the application to load any resources
     * <p>
     * Should result in calling either ready or finish, but possibly after a while, depending on
     * the success or failure of initialisation.
     * <p>
     * The parameters is an optional (i.e. possibly null) piece of data.
     *
     * @param parameters
     */
    public abstract void onCreate(Bundle parameters);

    /**
     * Called once the app is in the ready state to indicate that it's going to be transitioned to
     * immediately.
     */
    public abstract void onStart();

    /**
     * Called to indicate the app needs to be destroyed.
     * <p>
     * This could be because the app has been transitioned away or it could have never been shown
     * at all.
     */
    public abstract void onDestroy();

    /**
     * Called to indicate the given message has been received for this application instance
     *
     * @param message
     */
    public void onMessageReceived(byte[] message) {
    }

    /**
     * Called when the instance needs to save state and return a byte array.
     *
     * @param parameters
     */
    public abstract void onSaveInstanceState(Bundle parameters);

    public abstract void onConfigurationChanged();

    /**
     * Call this to indicate that for whatever reason the app should be terminated. When the app
     * has been transitioned away from the system will call onDestroy.
     */
    public final void finish() {
        if (handler != null) {
            handler.removeMessages(MSG_START, this);
            sendMessage(MSG_DESTROY);
        } else {
            destroyImpl();
        }
    }

    /**
     * Call this to indicate that the app has successfully completed initialisation and is ready
     * to immediately start when notified.
     */
    public final void ready() {
        if (callback != null) {
            if (!readyNotified) {
                readyNotified = true;

                Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_DISPLAY);

                try {
                    Message m = handler.obtainMessage(MSG_START, this);
                    long delta = startTime - System.currentTimeMillis();
                    delta = Math.max(delta, 0);
                    handler.sendMessageDelayed(m, delta);

                    callback.ready();
                } catch (Throwable throwable) {
                }
            }
        }
    }

    /**
     * Called by subclass to immediately render the first frame
     * <p>
     * Example use is the image app, speeds up readiness
     */
    protected final void start() {
        try {
            handler.obtainMessage(MSG_START, this).sendToTarget();
        } catch (Throwable t) {
        }
    }

    public final IAppServiceEngine.Stub getStub() {
        return stub;
    }

    public void logCanvas(CanvasLogLevel level, String tag, String message) {
        if (callback != null) {
            try {
                callback.log(level.toString(), tag, message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void onError(String name, Throwable throwable) {
        StringBuilder description = new StringBuilder();

        if (throwable != null) {
            for (StackTraceElement s : throwable.getStackTrace()) {
                description.append(s.toString());
                description.append("\n");
            }
        }

        if (callback != null) {
            try {
                callback.error(name, description.toString());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
