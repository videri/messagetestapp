package com.videri.canvas.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public class CanvasOrchestrationMsgItem implements Parcelable {
    public final static int TypeUnknown = 1001;
    public final static int TypeBoolean = 0;
    public final static int TypeInt = 1;
    public final static int TypeString = 2;

    public int typeObj = TypeUnknown;
    public String valueString = "";
    public String description = "";


    public CanvasOrchestrationMsgItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeObj);
        dest.writeString(valueString);
        dest.writeString(description);
    }

    private CanvasOrchestrationMsgItem(Parcel source) {
        readParcel(source);
    }

    private void readParcel(Parcel source) {
        typeObj = source.readInt();
        valueString = source.readString();
        description = source.readString();
    }

    public static final Parcelable.Creator<CanvasOrchestrationMsgItem> CREATOR = new Parcelable.Creator<CanvasOrchestrationMsgItem>() {
        @Override
        public CanvasOrchestrationMsgItem createFromParcel(Parcel source) {
            return new CanvasOrchestrationMsgItem(source);
        }

        @Override
        public CanvasOrchestrationMsgItem[] newArray(int size) {
            return new CanvasOrchestrationMsgItem[size];
        }
    };

}
