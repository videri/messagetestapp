package com.videri.canvas.sdk;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Process;
import android.view.Choreographer;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Subclass of CanvasApplication for more classic
 * Android usage patterns, specifically use of the
 * UI framework.
 */

public abstract class CanvasActivity extends CanvasApplication implements Choreographer.FrameCallback {
    private CanvasAppViewContainer container;
    private boolean dirty;

    protected CanvasActivity(AppService appService, Bundle params) {
        super(appService, params);
    }

    protected void setContentView(int resourceId) {
        LayoutInflater li = (LayoutInflater) getService().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = li.inflate(resourceId, null);
        setContentView(view);
    }

    protected void setContentView(View view) {
        container = new CanvasAppViewContainer(getService(), view, getWidth(), getHeight(), getSurface());
    }

    protected View findViewById(int id) {
        View view = null;

        if (container != null) {
            view = container.findViewById(id);
        }

        return view;
    }

    @Override
    public void onStart() {
        container.doLayout();
        dirty = true;
        getService().getChoreographer().postFrameCallback(this);
        Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void doFrame(long frameTimeNanos) {
        if (isRunning()) {
            getService().getChoreographer().postFrameCallback(this);

            if (container != null) {
                if (getService() != null && getSurface() != null && getSurface().isValid()) {
                    try {
                        dirty &= container.doLayoutIfNeeded();

                        if (dirty && getSurface() != null && getSurface().isValid()) {
                            final Canvas c = getSurface().lockCanvas(null);

                            container.draw(c);

                            if (getSurface() != null && getSurface().isValid()) {
                                getSurface().unlockCanvasAndPost(c);
                            }
                        }
                    } catch (Throwable t) {
                        logCanvas(CanvasLogLevel.ERROR, "WHIMCRASH", t.toString());
                        while (t.getCause() != null) {
                            t = t.getCause();
                            logCanvas(CanvasLogLevel.ERROR, "WHIMCRASH", "Cause " + t.toString());
                        }
                        finish();
                    }
                }
            }
        }
    }
}
