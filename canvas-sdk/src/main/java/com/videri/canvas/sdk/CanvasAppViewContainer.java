package com.videri.canvas.sdk;

import android.graphics.Rect;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.ViewParent;
import android.widget.AbsoluteLayout;

public class CanvasAppViewContainer extends AbsoluteLayout {
    private int width;
    private int height;
    private View view;
    private boolean needsLayout;

    public CanvasAppViewContainer(AppService appService, View view, int width, int height, Surface surface) {
        super(appService);

        this.width = width;
        this.height = height;
        this.view = view;

        setLayoutParams(new AbsoluteLayout.LayoutParams(width, height, 0, 0));

        addView(view);

        onSizeChanged(width, height, 0, 0);
        measure(View.MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        layout(0, 0, width, height);

        doLayout();
    }

    @Override
    public void requestLayout() {
        Log.v("WHIMCAN", "Layout requested");
        needsLayout = true;
        super.requestLayout();
    }

    @Override
    public ViewParent invalidateChildInParent(int[] location, Rect dirty) {
        Log.v("WHIMCAN", "Invalidation requested");
        needsLayout = true;
        return super.invalidateChildInParent(location, dirty);
    }

    boolean doLayoutIfNeeded() {
        if (needsLayout) {
            needsLayout = false;
            doLayout();

            return true;
        }

        return false;
    }

    void doLayout() {
        view.measure(View.MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        view.layout(0, 0, width, height);
        invalidate();
    }

    @Override
    public int getMinimumWidth() {
        return width;
    }

    @Override
    public int getMinimumHeight() {
        return height;
    }
}